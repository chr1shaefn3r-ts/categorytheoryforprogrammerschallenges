# Category Theory For Programmers Challenges

Typescript for the coding related challenges in:
Category Theory for Programmers by Bartosz Milewski

All coding is supposed to be run with [deno]([https://deno.land/).

Run all unit tests with `deno test`.

