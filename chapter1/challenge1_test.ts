import {
  assertStrictEquals,
} from "https://deno.land/std/testing/asserts.ts";

import {
  identity,
  identityAsArrowFunction,
  identityWithTypeGenerics,
} from "./challenge1.ts";

Deno.test("identity", () => {
  const mockObject = {};
  assertStrictEquals(identity(mockObject), mockObject);
});

Deno.test("identityAsArrowFunction", () => {
  const mockObject = {};
  assertStrictEquals(identityAsArrowFunction(mockObject), mockObject);
});

Deno.test("identityWithTypeGenerics", () => {
  const mockObject = {};
  assertStrictEquals(identityWithTypeGenerics(mockObject), mockObject);
  assertStrictEquals(identityWithTypeGenerics<object>(mockObject), mockObject);
});
