// Implement the composition function in your favourite language.
// It takes two functions as arguments and returns a function that is their composition

export const compose = (fnFirst: Function, fnSecond: Function) => (x: any) => fnFirst(fnSecond(x))
