// Implement, as best as you can, the identity function in your favorite language

export function identity(a: any) {
  return a;
}

export const identityAsArrowFunction = (a: any) => a;

export function identityWithTypeGenerics<T>(a: T) {
  return a;
}
