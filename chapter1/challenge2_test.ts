// Write a program that tries to test that your composition function respects identity. (Challenge 3)

import {
  assertStrictEquals,
} from "https://deno.land/std/testing/asserts.ts";

import {
  compose,
} from "./challenge2.ts";
import {
  identity,
} from "./challenge1.ts";

Deno.test("compose (right identity)", () => {
  const exampleMethod = (x: number) => x * x;
  const composedMethod = compose(exampleMethod, identity);

  // composedMethod still behaves like exampleMethod
  [-2, 0, +2, 42].forEach((testValue: number) => {
    assertStrictEquals(exampleMethod(testValue), composedMethod(testValue));
  });
});

Deno.test("compose (left identity)", () => {
  const exampleMethod = (x: number) => x * x;
  const composedMethod = compose(identity, exampleMethod);

  // composedMethod still behaves like exampleMethod
  [-2, 0, +2, 42].forEach((testValue: number) => {
    assertStrictEquals(exampleMethod(testValue), composedMethod(testValue));
  });
});
